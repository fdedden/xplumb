CC=gcc

PROJ=xplumb
LIBS=-lX11
CFLAGS=-g -Wall

$PROJ: xplumb.o
	$CC $CFLAGS -o $PROJ $prereq $LIBS

%.o: %.c
	$CC $CFLAGS -c $stem.c

clean:
	rm -f *.o $PROJ
